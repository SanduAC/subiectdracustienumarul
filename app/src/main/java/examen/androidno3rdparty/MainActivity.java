package examen.androidno3rdparty;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import examen.androidno3rdparty.database.DatabaseHelper;
import examen.androidno3rdparty.database.PackageDBHelper;
import examen.androidno3rdparty.models.DataPackage;
import examen.androidno3rdparty.models.DataPackagesForIntent;
import examen.androidno3rdparty.server.Client;
import examen.androidno3rdparty.tools.Constants;
import examen.androidno3rdparty.ui.ChartActivity;
import examen.androidno3rdparty.ui.CreatePackageActivity;
import examen.androidno3rdparty.ui.ListActivity;
import examen.androidno3rdparty.ui.common.ActionBarActivity;

public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_main);
        super.onCreate(savedInstanceState);
        setControls();
    }

    private void setControls() {
        findViewById(R.id.btn_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeActivityWithAnimantionAndPackagesExtra(CreatePackageActivity.class);
            }
        });
        findViewById(R.id.btn_list).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeActivityWithAnimantionAndPackagesExtra(ListActivity.class);
            }
        });
        findViewById(R.id.btn_backup).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PackageDBHelper.saveInDB(MainActivity.this, packagesList);
            }
        });
        findViewById(R.id.btn_update).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Thread thread = new Thread() {
                    @Override
                    public void run() {

                        Client.getPackagesFromServer(new Client.GetPackagesFromServerCallBack() {
                            @Override
                            public void success(List<DataPackage> dataPackages) {

                            }

                            @Override
                            public void fail(final String errorMessage) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        shortText(errorMessage);
                                    }
                                });
                            }
                        });
                    }
                };
                thread.start();
            }
        });
        findViewById(R.id.btn_rate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeActivityWithAnimantionAndPackagesExtra(ChartActivity.class);
            }
        });
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        packagesList=new ArrayList<>();
        packagesList.addAll(((DataPackagesForIntent)intent.getSerializableExtra(Constants.PACKAGES_LIST_ARGUMENT)).dataPackages);
        changeActivityWithAnimantionAndPackagesExtra(ListActivity.class);
    }
}
