package examen.androidno3rdparty.models;

import java.io.Serializable;
import java.util.Calendar;

public class DataPackage implements Serializable {

    public enum PackageType {
        position,
        state
    }

    private static int counter = 0;

    public int packageId;
    public PackageType packageType;
    public double latitude;
    public double longitude;
    public long timestamp;


    public DataPackage(PackageType packageType, double latitude, double longitude, long timestamp) {
        this.packageId = counter;
        counter += 1;
        this.packageType = packageType;
        this.latitude = latitude;
        this.longitude = longitude;
        this.timestamp = Calendar.getInstance().getTimeInMillis();
    }

    public DataPackage() {
        this.packageId = counter;
        counter += 1;
        this.timestamp = Calendar.getInstance().getTimeInMillis();
    }

    public void edit(DataPackage dataPackage) {
        packageType = dataPackage.packageType;
        latitude = dataPackage.latitude;
        longitude = dataPackage.longitude;
    }
}
